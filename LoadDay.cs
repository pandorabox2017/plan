﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;

namespace PlanLoad
{
    public class LoadDay
    {
        public DateTime Day { get; set; }
        public int AllMachines { get; set; }
        public List<LoadItem> LoadItems { get; set; } = new List<LoadItem>();
    }
}
