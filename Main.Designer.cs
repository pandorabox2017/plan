﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
namespace PlanLoad
{
    sealed partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            _labelMachinesFont?.Dispose();
            _labelNameFont?.Dispose();
            _labelNameFontBack?.Dispose();
            _selectOperation?.Dispose();
            strFormat.Dispose();

            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelProcess = new System.Windows.Forms.Panel();
            this.pbProcess = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.numericScaling = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.pbLoaded = new System.Windows.Forms.PictureBox();
            this.panelLoaded = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.экспортToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.диаграммаГанттаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.диаграммаЗагруженностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.синхронизироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отправитToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.диаграммыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.построитьДЗагруженностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.экспортГанттаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.экспортЗагруженностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sfdExcl = new System.Windows.Forms.SaveFileDialog();
            this.panelProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbProcess)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericScaling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoaded)).BeginInit();
            this.panelLoaded.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelProcess
            // 
            this.panelProcess.AutoScroll = true;
            this.panelProcess.BackColor = System.Drawing.Color.White;
            this.panelProcess.Controls.Add(this.pbProcess);
            this.panelProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProcess.Location = new System.Drawing.Point(0, 0);
            this.panelProcess.Name = "panelProcess";
            this.panelProcess.Size = new System.Drawing.Size(524, 201);
            this.panelProcess.TabIndex = 0;
            // 
            // pbProcess
            // 
            this.pbProcess.BackColor = System.Drawing.Color.White;
            this.pbProcess.Location = new System.Drawing.Point(0, 0);
            this.pbProcess.Name = "pbProcess";
            this.pbProcess.Size = new System.Drawing.Size(100, 50);
            this.pbProcess.TabIndex = 0;
            this.pbProcess.TabStop = false;
            this.pbProcess.Paint += new System.Windows.Forms.PaintEventHandler(this.pbProcess_Paint);
            this.pbProcess.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PbProcess_MouseDown);
            this.pbProcess.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PbProcess_MouseMove);
            this.pbProcess.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbProcess_MouseUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.numericScaling);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 80);
            this.panel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(270, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(231, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Сохранить диаграмму загруженности";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(270, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(231, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Построить диаграмму загруженности";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Action_DrawLoadDiagram_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(127, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(137, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Импорт данных";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Action_ImportFromFile_Click);
            // 
            // numericScaling
            // 
            this.numericScaling.Location = new System.Drawing.Point(9, 22);
            this.numericScaling.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericScaling.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericScaling.Name = "numericScaling";
            this.numericScaling.Size = new System.Drawing.Size(76, 20);
            this.numericScaling.TabIndex = 1;
            this.numericScaling.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericScaling.ValueChanged += new System.EventHandler(this.NumericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Масштаб:";
            // 
            // pbLoaded
            // 
            this.pbLoaded.Location = new System.Drawing.Point(0, 0);
            this.pbLoaded.Name = "pbLoaded";
            this.pbLoaded.Size = new System.Drawing.Size(100, 50);
            this.pbLoaded.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbLoaded.TabIndex = 0;
            this.pbLoaded.TabStop = false;
            this.pbLoaded.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PblLoaded_MouseDown);
            this.pbLoaded.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PbLoaded_MouseMove);
            this.pbLoaded.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbLoaded_MouseUp);
            // 
            // panelLoaded
            // 
            this.panelLoaded.AutoScroll = true;
            this.panelLoaded.BackColor = System.Drawing.Color.White;
            this.panelLoaded.Controls.Add(this.pbLoaded);
            this.panelLoaded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLoaded.Location = new System.Drawing.Point(0, 0);
            this.panelLoaded.Name = "panelLoaded";
            this.panelLoaded.Size = new System.Drawing.Size(524, 100);
            this.panelLoaded.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 104);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelProcess);
            this.splitContainer1.Panel1MinSize = 50;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panelLoaded);
            this.splitContainer1.Panel2MinSize = 50;
            this.splitContainer1.Size = new System.Drawing.Size(524, 307);
            this.splitContainer1.SplitterDistance = 201;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 3;
            // 
            // ofd
            // 
            this.ofd.Filter = "Excel files|*.xlsx|CSV|*.csv";
            this.ofd.Title = "Импортировать данные";
            // 
            // sfd
            // 
            this.sfd.Filter = "Image PNG|*.png";
            this.sfd.Title = "Сохранить изображение";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.бДToolStripMenuItem,
            this.диаграммыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(524, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.импортДанныхToolStripMenuItem,
            this.экспортToolStripMenuItem,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // импортДанныхToolStripMenuItem
            // 
            this.импортДанныхToolStripMenuItem.Name = "импортДанныхToolStripMenuItem";
            this.импортДанныхToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.импортДанныхToolStripMenuItem.Text = "Импорт";
            this.импортДанныхToolStripMenuItem.Click += new System.EventHandler(this.Action_ImportFromFile_Click);
            // 
            // экспортToolStripMenuItem
            // 
            this.экспортToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.диаграммаГанттаToolStripMenuItem,
            this.диаграммаЗагруженностиToolStripMenuItem});
            this.экспортToolStripMenuItem.Name = "экспортToolStripMenuItem";
            this.экспортToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.экспортToolStripMenuItem.Text = "Экспорт данных";
            // 
            // диаграммаГанттаToolStripMenuItem
            // 
            this.диаграммаГанттаToolStripMenuItem.Name = "диаграммаГанттаToolStripMenuItem";
            this.диаграммаГанттаToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.диаграммаГанттаToolStripMenuItem.Text = "Диаграмма Гантта";
            this.диаграммаГанттаToolStripMenuItem.Click += new System.EventHandler(this.Action_ExportGanttToFile_Click);
            // 
            // диаграммаЗагруженностиToolStripMenuItem
            // 
            this.диаграммаЗагруженностиToolStripMenuItem.Name = "диаграммаЗагруженностиToolStripMenuItem";
            this.диаграммаЗагруженностиToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.диаграммаЗагруженностиToolStripMenuItem.Text = "Диаграмма загруженности";
            this.диаграммаЗагруженностиToolStripMenuItem.Click += new System.EventHandler(this.Action_ExportLoadToFile_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(160, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // бДToolStripMenuItem
            // 
            this.бДToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.синхронизироватьToolStripMenuItem,
            this.отправитToolStripMenuItem});
            this.бДToolStripMenuItem.Name = "бДToolStripMenuItem";
            this.бДToolStripMenuItem.Size = new System.Drawing.Size(34, 20);
            this.бДToolStripMenuItem.Text = "БД";
            // 
            // синхронизироватьToolStripMenuItem
            // 
            this.синхронизироватьToolStripMenuItem.Name = "синхронизироватьToolStripMenuItem";
            this.синхронизироватьToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.синхронизироватьToolStripMenuItem.Text = "Синхронизировать";
            // 
            // отправитToolStripMenuItem
            // 
            this.отправитToolStripMenuItem.Name = "отправитToolStripMenuItem";
            this.отправитToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.отправитToolStripMenuItem.Text = "Отправить";
            // 
            // диаграммыToolStripMenuItem
            // 
            this.диаграммыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.построитьДЗагруженностиToolStripMenuItem,
            this.экспортГанттаToolStripMenuItem,
            this.экспортЗагруженностиToolStripMenuItem});
            this.диаграммыToolStripMenuItem.Name = "диаграммыToolStripMenuItem";
            this.диаграммыToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.диаграммыToolStripMenuItem.Text = "Диаграммы";
            // 
            // построитьДЗагруженностиToolStripMenuItem
            // 
            this.построитьДЗагруженностиToolStripMenuItem.Name = "построитьДЗагруженностиToolStripMenuItem";
            this.построитьДЗагруженностиToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.построитьДЗагруженностиToolStripMenuItem.Text = "Построить д-у загруженности";
            this.построитьДЗагруженностиToolStripMenuItem.Click += new System.EventHandler(this.Action_DrawLoadDiagram_Click);
            // 
            // экспортГанттаToolStripMenuItem
            // 
            this.экспортГанттаToolStripMenuItem.Name = "экспортГанттаToolStripMenuItem";
            this.экспортГанттаToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.экспортГанттаToolStripMenuItem.Text = "Экспорт Гантта";
            this.экспортГанттаToolStripMenuItem.Click += new System.EventHandler(this.Action_ExportGanttToImage_Click);
            // 
            // экспортЗагруженностиToolStripMenuItem
            // 
            this.экспортЗагруженностиToolStripMenuItem.Name = "экспортЗагруженностиToolStripMenuItem";
            this.экспортЗагруженностиToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.экспортЗагруженностиToolStripMenuItem.Text = "Экспорт загруженности";
            this.экспортЗагруженностиToolStripMenuItem.Click += new System.EventHandler(this.Button3_Click);
            // 
            // sfdExcl
            // 
            this.sfdExcl.Filter = "Excel files|*.xlsx|CSV|*.csv";
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 411);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(540, 450);
            this.Name = "Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDownAndUp);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDownAndUp);
            this.panelProcess.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbProcess)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericScaling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoaded)).EndInit();
            this.panelLoaded.ResumeLayout(false);
            this.panelLoaded.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelProcess;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown numericScaling;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pbLoaded;
        private System.Windows.Forms.Panel panelLoaded;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.PictureBox pbProcess;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem экспортToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem диаграммаЗагруженностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem диаграммаГанттаToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem синхронизироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отправитToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem диаграммыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem построитьДЗагруженностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem экспортГанттаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem экспортЗагруженностиToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog sfdExcl;
    }
}

