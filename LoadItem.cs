﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Drawing;

namespace PlanLoad
{
    public class LoadItem : IDisposable
    {
        private Color _colorOperation;
        public Color ColorOperation
        {
            get => _colorOperation;
            set
            {
                _colorOperation = value;
                Brush = new SolidBrush(value);
            }
        }
        public Brush Brush { get; set; }
        public int CountMachines { get; set; }
        public string OperationName { get; set; }

        public void Dispose()
        {
            Brush.Dispose();
        }
    }
}
