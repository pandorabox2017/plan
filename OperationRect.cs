﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Drawing;
using System.Windows.Forms;

namespace PlanLoad
{
    public class OperationRect : IDisposable
    {
        private int _scalePoint;
        public int ScalePoint
        {
            get => _scalePoint;
            set
            {
                _scalePoint = value;
                Height = _scalePoint * 2;
                Width = DayCount * _scalePoint;
                RectangleMain.Width = RectangleOperation.Width;
                RectangleAdditional.Height = _scalePoint;
                RectangleAdditional.Width = AdditionalDays * _scalePoint;
                if (NecessaryMachine)
                {
                    RectangleMain.Y = RectangleOperation.Y + _scalePoint;
                    RectangleMain.Height = _scalePoint;
                }
                else
                    RectangleMain.Height = Height;
            }
        }


        // Наименование операции
        public string OperationName { get; }
        // Объём работы
        public int VolumeWork { get; }
        // Дата начала
        public DateTime StartDay { get; set; }

        // Дата окончания
        public DateTime EndDay { get; set; }

        // Норма выработки - скорость смены
        public int ShiftRate { get; }

        // Количество дней
        public int DayCount { get; private set; }
        // Кол-во машин
        public int MachineUnits { get; set; }
        // Объём основной работы
        public double MainVolumeWork { get; set; }


        // Объём дополнительной работы
        public double AdditionalVolumeWork { get; set; }
        // Кол-во дней на дополнительную машину
        public int AdditionalDays { get; set; }

        // Необходимо машину сверху
        public bool NecessaryMachine { get; }

        public DateTime StartAdditionalDate { get; set; }
        public DateTime EndAdditionalDate { get; set; }
        private int _startAdditionalDay;
        public int StartAdditionalDay
        {
            get => _startAdditionalDay;
            set
            {
                if (value < 0 || !NecessaryMachine || (StartDay.AddDays(value + AdditionalDays - 1) > EndDay))
                    return;
                _startAdditionalDay = value;
                StartAdditionalDate = StartDay.AddDays(value);
                EndAdditionalDate = StartAdditionalDate.AddDays(AdditionalDays - 1);
            }
        }

        private DateTime _startMainDay;
        public DateTime StartMainDay
        {
            get => _startMainDay;
            set
            {
                _startMainDay = value;

                StartDay = value;
                EndDay = value.AddDays(DayCount - 1);
            }
        }

        public float X
        {
            get => RectangleOperation.X;
            set
            {
                RectangleMain.X = value;
                RectangleMainLabel.X = value + 3;

                RectangleOperation.X = value;

                RectangleAdditional.X = value + StartAdditionalDay * ScalePoint;
                RectangleAdditionalLabel.X = value + StartAdditionalDay * ScalePoint + 3;

                RectangleOperationNameBack.X = RectangleMain.X + (RectangleMain.Width - RectangleOperationNameBack.Width) / 2;
                RectangleOperationName.X = RectangleOperationNameBack.X - 2;
            }
        }
        public float Y
        {
            get => RectangleOperation.Y;
            set
            {
                RectangleOperation.Y = value;
                RectangleMain.Y = value;
                RectangleAdditional.Y = value;

                if (NecessaryMachine)
                    RectangleMain.Y += ScalePoint;

                // Операции на менее чем 2 дня, занимают слишком мало место для лэйблов
                var resourceRect = DayCount <= 2 ? ScalePoint : RectangleMain.Height;
                var nameRect = DayCount <= 2 ? RectangleMain.Height + ScalePoint : RectangleMain.Height;

                RectangleMainLabel.Y = RectangleMain.Y + (resourceRect - RectangleMainLabel.Height) / 2;
                RectangleAdditionalLabel.Y = RectangleAdditional.Y + (RectangleAdditional.Height - RectangleAdditionalLabel.Height) / 2;
                RectangleOperationNameBack.Y = RectangleMain.Y + (nameRect - RectangleOperationNameBack.Height) / 2;
                RectangleOperationName.Y = RectangleOperationNameBack.Y - 2;
            }
        }
        public float Width
        {
            get => RectangleOperation.Width;
            set => RectangleOperation.Width = value;
        }
        public float Height
        {
            get => RectangleOperation.Height;
            set => RectangleOperation.Height = value;
        }

        private Font MachineFont { get; set; }
        private Font NameFont { get; set; }
        private Font NameFontBack { get; set; }
        private Brush Brush;
        public RectangleF RectangleOperation;
        public RectangleF RectangleAdditional;
        public RectangleF RectangleMain;

        public RectangleF RectangleAdditionalLabel;
        public RectangleF RectangleMainLabel;

        public RectangleF RectangleOperationName;
        public RectangleF RectangleOperationNameBack;


        public Color ColorOperation { get; set; }

        public OperationRect(string operationName, int volumeWork, DateTime startDay, DateTime endDay, int shiftRate, Font machineFont, Font nameFont, Font nameFontBack, int stepPoint = 40)
        {
            OperationName = operationName;
            VolumeWork = volumeWork;
            StartMainDay = startDay;
            EndDay = endDay;
            ShiftRate = shiftRate;

            DayCount = (int)(endDay - startDay).TotalDays + 1;
            var tmpMachines = (double)volumeWork / (DayCount * ShiftRate);
            MachineUnits = (int)tmpMachines;
            MainVolumeWork = DayCount * ShiftRate * MachineUnits;

            if (MainVolumeWork >= 1)
            {
                if (tmpMachines % 1 <= 0.84 && tmpMachines % 1 > 0)
                {
                    NecessaryMachine = true;
                }
                else
                {// Округляем до монолитной операции, если дробная часть выше 0,84
                    NecessaryMachine = false;
                    MachineUnits++;
                    MainVolumeWork = DayCount * ShiftRate * MachineUnits;
                }
            }
            else
            {
                NecessaryMachine = false;
                MainVolumeWork = DayCount * ShiftRate * tmpMachines;
                MachineUnits = 1;
            }
            ScalePoint = stepPoint;

            Width = DayCount * ScalePoint;
            Height = 2 * ScalePoint;

            RectangleMainLabel.Size = TextRenderer.MeasureText($"{MachineUnits} т.", machineFont);
            RectangleAdditionalLabel.Size = TextRenderer.MeasureText($"1 т.", machineFont);
            RectangleOperationName.Size = TextRenderer.MeasureText(OperationName, nameFont);
            RectangleOperationNameBack.Size = TextRenderer.MeasureText(OperationName, nameFontBack);

            var color = StringColor.BuildToRgb(GetHashCode().ToString());
            ColorOperation = color;
            Brush = new SolidBrush(color);
            MachineFont = machineFont;
            NameFont = nameFont;
            NameFontBack = nameFontBack;

            if (NecessaryMachine)
            {
                AdditionalVolumeWork = VolumeWork - MainVolumeWork;
                AdditionalDays = (int)Math.Ceiling(AdditionalVolumeWork / ShiftRate);
                StartAdditionalDay = 0;

                RectangleAdditional.X = RectangleOperation.X;
                RectangleAdditional.Y = RectangleOperation.Y;
                RectangleAdditional.Height = ScalePoint;
                RectangleAdditional.Width = AdditionalDays * ScalePoint;

                RectangleMain.X = RectangleAdditional.X;
                RectangleMain.Y = RectangleOperation.Y + ScalePoint;
                RectangleMain.Width = RectangleOperation.Width;
                RectangleMain.Height = ScalePoint;
            }
            else
            {
                RectangleMain = RectangleOperation;
            }
        }

        public void SetAdditionalDateOffset(DateTime? dateTime)
        {
            if (!(dateTime is DateTime dt)) return;

            var diffDay = (int)(dt - StartDay).TotalDays;
            StartAdditionalDay = diffDay;
        }

        public void DrawOnGraphics(Graphics gfx)
        {
            gfx.FillRectangle(Brush, RectangleMain);
            gfx.FillRectangle(Brushes.White, RectangleMainLabel);
            gfx.DrawString($"{MachineUnits} т.", MachineFont, Brushes.Black, RectangleMainLabel.Location);
            gfx.DrawString($"{OperationName}", NameFontBack, Brushes.Black, RectangleOperationNameBack.Location);
            gfx.DrawString($"{OperationName}", NameFont, Brushes.White, RectangleOperationName.Location);

            if (NecessaryMachine)
            {
                gfx.FillRectangle(Brush, RectangleAdditional);
                gfx.DrawLine(pen: Pens.Black,
                    x1: RectangleAdditional.X,
                    y1: RectangleAdditional.Y + ScalePoint,
                    x2: RectangleAdditional.X + RectangleAdditional.Width,
                    y2: RectangleAdditional.Y + ScalePoint);

                gfx.FillRectangle(Brushes.White, RectangleAdditionalLabel);
                gfx.DrawString($"1 т.", MachineFont, Brushes.Black, RectangleAdditionalLabel.Location);
            }
        }

        public sealed override int GetHashCode()
        {
            unchecked
            {
                var hash = (int)2166136261;
                hash = (hash * 16777619) ^ OperationName    .GetHashCode();
                hash = (hash * 16777619) ^ VolumeWork       .GetHashCode();
                hash = (hash * 16777619) ^ StartDay         .GetHashCode();
                hash = (hash * 16777619) ^ EndDay           .GetHashCode();
                hash = (hash * 16777619) ^ ShiftRate        .GetHashCode();
                hash = (hash * 16777619) ^ NecessaryMachine .GetHashCode();
                hash = (hash * 16777619) ^ X                .GetHashCode();
                hash = (hash * 16777619) ^ Y                .GetHashCode();
                hash = (hash * 16777619) ^ Width            .GetHashCode();
                hash = (hash * 16777619) ^ Height           .GetHashCode();
                return hash;
            }
        }

        public void Dispose()
        {
            Brush.Dispose();
            MachineFont.Dispose();
            NameFont.Dispose();
            NameFontBack.Dispose();
        }
    }
}
