﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using OfficeOpenXml;
using OfficeOpenXml.ConditionalFormatting;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlanLoad
{
    //TODO: Экспорт данных в формате xlsx
    public sealed partial class Main : Form
    {
        private readonly Font _labelMachinesFont;
        private readonly Font _labelNameFont;
        private readonly Font _labelNameFontBack;

        #region Process diagram
        private List<OperationRect> _operationRects = new List<OperationRect>();

        private const int LeftMarginProcessDiagram = 40;
        private const int BottomMarginProcessDiagram = 40;

        private int _heightProcessDiagram;
        private int _widthProcessDiagram;
        private DateTime _leftStartDay;
        private DateTime _rightEndDay;
        private int _countDays;

        private Rectangle[] _rectDayLabels;
        private Size _sizeDayLabel;
        #endregion

        private int _scalePoint = 40;
        private int ScalePoint
        {
            get => _scalePoint;
            set
            {
                if (value < 40 || value > 200) return;
                numericScaling.Value = value;
                _scalePoint = value;
                RescalingProcessDiagram();
            }
        }

        private bool _isCtrlDown;

        private StringFormat strFormat = new StringFormat();

        public Main()
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            Text = Properties.Resources.Header;

            strFormat.LineAlignment = StringAlignment.Center;
            strFormat.Alignment = StringAlignment.Center;

            pbProcess.MouseWheel += PbProcess_MouseWheel;
            panelProcess.MouseWheel += PbProcess_MouseWheel;
            
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor, true);
            _labelMachinesFont = new Font(label1.Font.FontFamily, 9f);
            _labelNameFont = new Font(label1.Font.FontFamily, 12f, FontStyle.Bold);
            _labelNameFontBack = new Font(label1.Font.FontFamily, 12f, FontStyle.Bold);
            _sizeDayLabel = TextRenderer.MeasureText("77.77", _labelMachinesFont);
        }

        private void PbProcess_MouseWheel(object sender, MouseEventArgs e)
        {
            if (_isCtrlDown && Math.Abs(e.Delta) == 120)
            {
                ScalePoint += e.Delta / 60;
                ((HandledMouseEventArgs)e).Handled = true;
            }
        }


        private void Main_KeyDownAndUp(object sender, KeyEventArgs e)
        {
            _isCtrlDown = e.Control;
        }

        private async void Action_ImportFromFile_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() != DialogResult.OK) return;
            _isUpdateOnImport = true;

            await ImportFromExcel_v2(ofd.FileName);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            _isUpdateOnImport = false;

            pbProcess.Invalidate();
        }

        // При импорте могут быть изменения в списке из-за сортировки
        private bool _isUpdateOnImport;

        // TODO: Загрузка данных в отдельном потоке
        private async Task ImportFromExcel_v2(string path)
        {
            if (!File.Exists(path))
            {
                MessageBox.Show("Файл не существует по указанному пути", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            _operationRects.Clear();

            var file = new FileInfo(path);

            var row = 0;
            ExcelPackage package = new ExcelPackage();
            try
            {
                ExcelWorksheet ws;
                if (Path.GetExtension(path) == ".csv")
                {
                    var format = new ExcelTextFormat
                    {
                        Delimiter = ';',
                        Culture = CultureInfo.GetCultureInfo("ru-RU"),
                        Encoding = new UTF8Encoding()
                    };

                    ws = package.Workbook.Worksheets.Add("1");
                    await ws.Cells[1, 1].LoadFromTextAsync(file, format);
                }
                else
                {
                    await package.LoadAsync(file);
                    ws = package.Workbook.Worksheets.First();
                }

                
                var culture = CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat;
                var list = ws.Cells[ws.Dimension.Start.Row + 1, ws.Dimension.Start.Column,
                                    ws.Dimension.End.Row, ws.Dimension.Start.Column + 4];

                var startRow = ws.Dimension.Start.Row + 1;
                var startCol = ws.Dimension.Start.Column;

                // Явно указываем формат даты для Linux Mono
                list[1, startCol + 2, ws.Dimension.End.Row, startCol + 3].Style.Numberformat.Format = "dd.MM.yyyy";
                list[1, startCol + 5, ws.Dimension.End.Row, startCol + 5].Style.Numberformat.Format = "dd.MM.yyyy";

                for (var i = startRow; i <= ws.Dimension.End.Row; i++)
                {
                    row = i;

                    var opName = ws.Cells[i, startCol].Text;
                    if (string.IsNullOrWhiteSpace(opName))
                        break;

                    var opVol           = int.Parse(list[i, startCol + 1].Text);
                    var opStart     = DateTime.Parse(list[i, startCol + 2].Text, culture);
                    var opEnd       = DateTime.Parse(list[i, startCol + 3].Text, culture);
                    var opShiftRate     = int.Parse(list[i, startCol + 4].Text);

                    // Смещённое поле дополнительного ресурса
                    var offsetDate   = DateTime.TryParse(list[i, startCol + 5].Text, out DateTime dt) ? (DateTime?)dt : null;

                    var operation = new OperationRect(opName, opVol, opStart, opEnd, opShiftRate, _labelMachinesFont, _labelNameFont, _labelNameFontBack, ScalePoint);
                    operation.SetAdditionalDateOffset(offsetDate);

                    _operationRects.Add(operation);
                }
                

                _leftStartDay = _operationRects.Min(x => x.StartDay);
                _rightEndDay  = _operationRects.Max(x => x.EndDay);
                _countDays = (int)(_rightEndDay - _leftStartDay).TotalDays + 1;

                _heightProcessDiagram = _operationRects.Count * ScalePoint * 2 + BottomMarginProcessDiagram;
                _widthProcessDiagram = _countDays * ScalePoint + LeftMarginProcessDiagram;

                pbProcess.Width = _widthProcessDiagram + 1;
                pbProcess.Height = _heightProcessDiagram + 1;

                _operationRects = _operationRects.OrderBy(x => x.StartDay).ToList();

                Parallel.For(0, _operationRects.Count, i =>
                {
                    var operation = _operationRects[i];
                    var startDay = (int)(operation.StartDay - _leftStartDay).TotalDays;
                    var left = LeftMarginProcessDiagram + startDay * ScalePoint;
                    var top = _heightProcessDiagram - (i * ScalePoint * 2) - ScalePoint * 2 - BottomMarginProcessDiagram;

                    operation.X = left;
                    operation.Y = top;
                });

                _delimiterDayProcessOperation = new int[_countDays];
                _rectDayLabels = new Rectangle[_countDays];

                Parallel.For(0, _countDays, i =>
                {
                    _delimiterDayProcessOperation[i] = i * ScalePoint + LeftMarginProcessDiagram;
                    _rectDayLabels[i] = new Rectangle
                    {
                        X = _delimiterDayProcessOperation[i] + (ScalePoint - _sizeDayLabel.Width) / 2,
                        Y = _heightProcessDiagram - BottomMarginProcessDiagram,
                        Width = ScalePoint,
                        Height = BottomMarginProcessDiagram
                    };
                });

                ws.Dispose();
            }
            catch (Exception ex)
            {
                _operationRects.Clear();
                MessageBox.Show($"Ошибка импорта\n\nRow = {row}\n{ex.Message}", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                package.Dispose();
            }

        }

        private void pbProcess_Paint(object sender, PaintEventArgs e)
        {
            if (_isUpdateOnImport || _operationRects.Count == 0) return;
            for (var i = 0; i < _operationRects.Count; i++)
            {
                _operationRects[i].DrawOnGraphics(e.Graphics);
            }
            DrawAxis(e.Graphics, _widthProcessDiagram, _heightProcessDiagram, LeftMarginProcessDiagram, BottomMarginProcessDiagram);
            var gfx = e.Graphics;
            for (var i = 0; i < _rectDayLabels.Length; i++)
            {
                var day = _leftStartDay.AddDays(i).ToString("dd.MM");

                gfx.DrawString(day, _labelMachinesFont, Brushes.Black, _rectDayLabels[i], strFormat);
                //TextRenderer.DrawText(gfx,
                //    day,
                //    _labelMachinesFont,
                //    _rectDayLabels[i],
                //    Color.Black,
                //    TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter | TextFormatFlags.EndEllipsis);
            }
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            ScalePoint = (int)numericScaling.Value;
        }

        private void TaskRescaleWorker_v2()
        {
            _heightProcessDiagram = _operationRects.Count * ScalePoint * 2 + BottomMarginProcessDiagram;
            _widthProcessDiagram = _countDays * ScalePoint + LeftMarginProcessDiagram;

            pbProcess.BeginInvoke((Action)(() =>
            {
                pbProcess.Width = _widthProcessDiagram + 1;
                pbProcess.Height = _heightProcessDiagram + 1;
            }));


            Parallel.For(0, _delimiterDayProcessOperation.Length, i =>
            {
                _delimiterDayProcessOperation[i] = i * ScalePoint + LeftMarginProcessDiagram;
                _rectDayLabels[i].X = _delimiterDayProcessOperation[i];
                _rectDayLabels[i].Y = _heightProcessDiagram - BottomMarginProcessDiagram;// + (BottomMarginProcessDiagram - _sizeDayLabel.Height) / 2;
                _rectDayLabels[i].Width = ScalePoint;
            });
            Parallel.For(0, _operationRects.Count, i =>
            {
                var operation = _operationRects[i];
                var startDay = (int)(operation.StartDay - _leftStartDay).TotalDays;
                var left = LeftMarginProcessDiagram + startDay * ScalePoint;
                var top = _heightProcessDiagram - (i * ScalePoint * 2) - ScalePoint * 2 - BottomMarginProcessDiagram;

                operation.ScalePoint = ScalePoint;
                operation.X = left;
                operation.Y = top;
            });

            pbProcess.Invalidate();
        }

        private Task _taskRescale;

        private void RescalingProcessDiagram()
        {
            // TODO: CancellationToken
            if (_operationRects.Count == 0)
                return;
            _taskRescale = new Task(TaskRescaleWorker_v2);
            _taskRescale.Start();
        }
        
        private void Action_DrawLoadDiagram_Click(object sender, EventArgs e)
        {
            try
            {
                if (_operationRects.Count > 0)
                {
                    var loads = GetDataLoad(_operationRects);

                    if (loads.Count > 0)
                    {
                        var maxMachines = loads.Max(x => x.AllMachines);
                        DrawLoadDiagram(loads, maxMachines, ScalePoint);
                    }
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private List<LoadDay> GetDataLoad(List<OperationRect> operations)
        {
            var loads = new List<LoadDay>(_countDays);

            for (var i = 0; i < _countDays; i++)
            {
                var day = _leftStartDay.AddDays(i);
                var loadDay = new LoadDay
                {
                    Day = day
                };
                int machines = 0;

                foreach (var operation in operations.Where(x => x.StartDay <= day && x.EndDay >= day))
                {
                    var loadItem = new LoadItem
                    {
                        ColorOperation = operation.ColorOperation,
                        CountMachines = operation.MachineUnits,
                        OperationName = operation.OperationName
                    };

                    if (operation.StartAdditionalDate <= day && operation.EndAdditionalDate >= day) loadItem.CountMachines++;
                    loadDay.LoadItems.Add(loadItem);

                    machines += loadItem.CountMachines;
                }
                loadDay.AllMachines = machines;
                loads.Add(loadDay);
            }

            return loads;
        }

        private void DrawLoadDiagram(List<LoadDay> loadeds, int maxMachines, int scale)
        {
            var rightDayInt = 0;

            for (var i = loadeds.Count - 1; i >= 0; i--)
                if (loadeds[i].AllMachines > 0)
                {
                    rightDayInt = i + 1;
                    break;
                }

            const int leftMargin = 40;
            const int bottomMargin = 20;

            var heightLoadDiagram = bottomMargin + maxMachines * ScalePoint;
            var widthLoadDiagram  = leftMargin + rightDayInt * ScalePoint;

            var bmp = new Bitmap(widthLoadDiagram + 1, heightLoadDiagram + 1);

            using (var gfx = Graphics.FromImage(bmp))
            using (var fnt = new Font(label1.Font.FontFamily, 9f, FontStyle.Regular))
            using (var fntCnt = new Font(label1.Font.FontFamily, 9f, FontStyle.Regular))
            using (var fntName = new Font(label1.Font.FontFamily, 9f, FontStyle.Bold))
            {
                gfx.FillRectangle(Brushes.White, 0, 0, bmp.Width, bmp.Height);

                gfx.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                for (var i = 0; i < rightDayInt; i++)
                {
                    var day = loadeds[i];
                    var left = leftMargin + (int) (day.Day - _leftStartDay).TotalDays * ScalePoint;


                    var load = 0;
                    for (var j = 0; j < day.LoadItems.Count; j++)
                    {
                        var item = day.LoadItems[j];
                        load += item.CountMachines;
                        var rect = new Rectangle
                        {
                            X = left,
                            Y = heightLoadDiagram - bottomMargin - load * scale,
                            Width = ScalePoint,
                            Height = item.CountMachines * scale
                        };

                        gfx.FillRectangle(item.Brush, rect);
                        gfx.DrawRectangle(Pens.Black, rect);

                        var size = gfx.MeasureString(item.OperationName, fntName);

                        var labelRect = new RectangleF
                        {
                            X = rect.X + (rect.Width - size.Width) / 2,
                            Y = rect.Y + (rect.Height - size.Height) / 2,
                            Width = ScalePoint,
                            Height = ScalePoint
                        };
                        gfx.DrawString(item.OperationName, fntName, Brushes.Black, labelRect);
                        labelRect.X -= 2;
                        labelRect.Y -= 2;
                        gfx.DrawString(item.OperationName, fntName, Brushes.White, labelRect);
                    }

                    var txtDay = day.Day.ToString("dd.MM");
                    var rectDay = new Rectangle();
                    rectDay.Height = bottomMargin;
                    rectDay.Width = ScalePoint;
                    rectDay.X = left;
                    rectDay.Y = heightLoadDiagram - bottomMargin;

                    gfx.DrawString(txtDay, fnt, Brushes.Black, rectDay, strFormat);
                }

                DrawAxis(gfx, bmp.Width, bmp.Height, leftMargin, bottomMargin);

                for (var i = 0; i < maxMachines; i++)
                {
                    var cnt = $"{maxMachines - i}";
                    var size = gfx.MeasureString(cnt, fntCnt);

                    gfx.DrawString(cnt, fntCnt, Brushes.Black, new RectangleF
                    {
                        X = (leftMargin - size.Width) / 2,
                        Y = i * ScalePoint + (ScalePoint - size.Height) / 2,
                        Height = size.Height,
                        Width = size.Width
                    });
                }

                pbLoaded.Image = bmp;
            }
        }

        private void DrawAxis(Graphics gfx, int width, int height, int left, int bottom)
        {
            using (var pen = new Pen(Color.Black) { DashStyle = DashStyle.Dot })
            {
                gfx.DrawLine(Pens.Black, left, 0, left, height);
                gfx.DrawLine(Pens.Black, 0, height - bottom, width, height - bottom);

                var countRows = (height - bottom) / ScalePoint;
                var countCols = (width - left) / ScalePoint;

                for (var i = 1; i < countRows; i++)
                {
                    var y = height - bottom - i * ScalePoint - 1;
                    gfx.DrawLine(pen, 0, y, width, y);
                }
                for (var i = 1; i <= countCols; i++)
                {
                    var x = i * ScalePoint + left;
                    gfx.DrawLine(pen, x, 0, x, height);
                }
            }
        }

        #region Operations moved

        private OperationRect _selectOperation;
        private bool _selectTopRect;
        private float _relativeDraggingX;
        private bool _dragOperation;
        private int[] _delimiterDayProcessOperation;

        private Point _startScrollProcess;
        private Point _startScrollLoad;


        private void PbProcess_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && GetOperationAt(e.Location) is OperationRect operation)
            {
                _selectOperation = operation;
                _relativeDraggingX = e.Location.X -
                    (_selectTopRect ? _selectOperation.RectangleAdditional.X : _selectOperation.X);
                _dragOperation = true;
                Cursor.Current = Cursors.SizeAll;
            }
            else if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
            {
                Cursor.Current = Cursors.NoMove2D;
                _startScrollProcess = e.Location;
            }
        }
        private void PblLoaded_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
            {
                _startScrollLoad = e.Location;
                Cursor = Cursors.NoMove2D;
            }
        }

        private OperationRect GetOperationAt(Point p)
        {
            // TODO: Можно сделать параллельным с CancellationToken Parallel.For
            for (var i = 0; i < _operationRects.Count; i++)
            {
                if (_operationRects[i].RectangleOperation.Contains(p))
                {
                    _selectTopRect = _operationRects[i].RectangleAdditional.Contains(p);
                    return _operationRects[i];
                }
            }
            return null;
        }

        private void PbProcess_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _dragOperation)
            {
                var currentRectX = e.Location.X - _relativeDraggingX;


                if (_selectTopRect)
                {
                    if (_selectOperation.RectangleOperation.X <= currentRectX &&
                        _selectOperation.RectangleOperation.X + _selectOperation.RectangleOperation.Width - _selectOperation.RectangleAdditional.Width
                        >= currentRectX)
                    {
                        var point = _delimiterDayProcessOperation.FirstOrDefault(x => x + ScalePoint / 2 > currentRectX);

                        _selectOperation.RectangleAdditional.X = point;
                        _selectOperation.RectangleAdditionalLabel.X = point + 3;

                        _selectOperation.StartAdditionalDay = (int)(point - _selectOperation.RectangleOperation.X) / ScalePoint;
                    }
                }
                else
                {
                    if (currentRectX >= LeftMarginProcessDiagram &&
                        currentRectX <= _widthProcessDiagram - _selectOperation.Width)
                    {
                        var point = _delimiterDayProcessOperation.FirstOrDefault(x => x + ScalePoint / 2 > currentRectX);
                        var currentDay = (point - LeftMarginProcessDiagram) / ScalePoint;


                        _selectOperation.StartMainDay = _leftStartDay.AddDays(currentDay);
                        _selectOperation.X = point;

                        if (_selectOperation.NecessaryMachine)
                            _selectOperation.StartAdditionalDay = (int)(_selectOperation.RectangleAdditional.X - _selectOperation.RectangleOperation.X) / ScalePoint;
                    }
                }
                pbProcess.Invalidate();
            }
            else if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
            {
                var changePoint = new Point(e.Location.X - _startScrollProcess.X,
                                            e.Location.Y - _startScrollProcess.Y);
                panelProcess.AutoScrollPosition = new Point(-panelProcess.AutoScrollPosition.X - changePoint.X,
                                                            -panelProcess.AutoScrollPosition.Y - changePoint.Y);
            }
        }
        private void PbLoaded_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
            {
                var changePoint = new Point(e.Location.X - _startScrollLoad.X,
                                            e.Location.Y - _startScrollLoad.Y);
                panelLoaded.AutoScrollPosition = new Point(-panelLoaded.AutoScrollPosition.X - changePoint.X,
                                                           -panelLoaded.AutoScrollPosition.Y - changePoint.Y);
            }
        }

        private void PbProcess_MouseUp(object sender, MouseEventArgs e)
        {
            _dragOperation = false;
            Cursor.Current = Cursors.Default;
            pbProcess.Invalidate();
        }
        private void PbLoaded_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
        }

        #endregion

        private void Button3_Click(object sender, EventArgs e)
        {
            if (pbLoaded.Image is Bitmap img && sfd.ShowDialog() == DialogResult.OK)
            {
                img.Save(sfd.FileName, ImageFormat.Png);
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Action_ExportGanttToImage_Click(object sender, EventArgs e)
        {
            if (_operationRects.Count == 0)
            {
                MessageBox.Show("Ошибка при выполнении экспорта диаграммы\nДанные отсутствуют!", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var bmp = new Bitmap(_widthProcessDiagram, _heightProcessDiagram);
                using (var gfx = Graphics.FromImage(bmp))
                {
                    gfx.FillRectangle(Brushes.White, 0, 0, bmp.Width, bmp.Height);

                    for (int i = 0; i < _operationRects.Count; i++)
                    {
                        _operationRects[i].DrawOnGraphics(gfx);
                    }
                    DrawAxis(gfx, _widthProcessDiagram, _heightProcessDiagram, LeftMarginProcessDiagram, BottomMarginProcessDiagram);
                    for (int i = 0; i < _rectDayLabels.Length; i++)
                    {
                        var day = _leftStartDay.AddDays(i).ToString("dd.MM");
                        gfx.DrawString(day, _labelMachinesFont, Brushes.Black, _rectDayLabels[i]);
                    }
                    bmp.Save(sfd.FileName, ImageFormat.Png);
                }
            }
        }

        private async void Action_ExportGanttToFile_Click(object sender, EventArgs e)
        {
            if (_operationRects.Count == 0)
            {
                MessageBox.Show("Данные отсутствуют!", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (sfdExcl.ShowDialog() == DialogResult.OK)
            {
                await Task.Run(() => ExportGanttToFile(_operationRects, sfdExcl.FileName));

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        async Task ExportGanttToFile(List<OperationRect> operations, string path)
        {
            FileStream file = null;
            var package = new ExcelPackage();
            var ws = package.Workbook.Worksheets.Add(DateTime.Now.ToString("dd.MM.yyyy"));

            try
            {
                file = File.Open(path, FileMode.Create, FileAccess.Write, FileShare.Read);

                ws.Cells[1, 1].Value = "№ технологической операции";
                ws.Column(1).Width = 17;
                ws.Cells[1, 2].Value = "Объём работы";
                ws.Column(2).Width = 16;
                ws.Cells[1, 3].Value = "Дата начала";
                ws.Column(3).Width = 13;
                ws.Cells[1, 4].Value = "Дата завершения";
                ws.Column(4).Width = 17;
                ws.Cells[1, 5].Value = "Норма выработки";
                ws.Column(5).Width = 21;
                ws.Cells[1, 6].Value = "Отложено дней доп ресурс";
                ws.Column(6).Width = 17;

                var cellsHeaderStyle = ws.Cells[1, 1, 1, 6].Style;
                cellsHeaderStyle.WrapText = true;
                cellsHeaderStyle.VerticalAlignment = ExcelVerticalAlignment.Center;

                var row = 2;

                foreach (var operation in operations.OrderBy(x => x.StartDay))
                {
                    ws.Cells[row, 1].Value = operation.OperationName;
                    ws.Cells[row, 2].Value = operation.VolumeWork;
                    ws.Cells[row, 3].Value = operation.StartDay.ToString("dd.MM.yyyy");
                    ws.Cells[row, 4].Value = operation.EndDay.ToString("dd.MM.yyyy");
                    ws.Cells[row, 5].Value = operation.ShiftRate;
                    if (operation.StartAdditionalDay > 0)
                        ws.Cells[row, 6].Value = operation.StartAdditionalDate.ToString("dd.MM.yyyy");
                    row += 1;
                }

                var cellsTableStyle = ws.Cells[1, 1, row - 1, 6].Style;
                cellsTableStyle.Border.Top.Style = ExcelBorderStyle.Thin;
                cellsTableStyle.Border.Left.Style = ExcelBorderStyle.Thin;
                cellsTableStyle.Border.Right.Style = ExcelBorderStyle.Thin;
                cellsTableStyle.Border.Bottom.Style = ExcelBorderStyle.Thin;
                cellsTableStyle.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                await package.SaveAsAsync(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка при экспорте:\n\n{ex.Message}", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                file?.Close();
                file?.Dispose();
                ws.Dispose();
                package.Dispose();
            }
        }

        private async void Action_ExportLoadToFile_Click(object sender, EventArgs e)
        {
            if (_operationRects.Count == 0)
            {
                MessageBox.Show("Данные отсутствуют!", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (sfdExcl.ShowDialog() == DialogResult.OK)
            {
                await Task
                    .Run(() => GetDataLoad(_operationRects))
                    .ContinueWith(async task =>
                    {
                        var days = task.Result;
                        FileStream file = null; 
                        var package = new ExcelPackage();
                        var ws = package.Workbook.Worksheets.Add(DateTime.Now.ToString("dd.MM.yyyy"));

                        try
                        {
                            file = File.Open(sfdExcl.FileName, FileMode.Create, FileAccess.Write, FileShare.Read);

                            ws.Cells[1, 1].Value = "День";
                            ws.Cells[1, 2].Value = "Объём требуемого ресурса";
                            var cellsHeaderStyle = ws.Cells[1, 1, 1, 2].Style;
                            cellsHeaderStyle.WrapText = true;
                            cellsHeaderStyle.VerticalAlignment = ExcelVerticalAlignment.Center;

                            var row = 2;

                            foreach (var day in days)
                            {
                                ws.Cells[row, 1].Value = day.Day.ToString("dd.MM.yyyy");
                                ws.Cells[row, 2].Value = day.AllMachines;

                                row += 1;
                            }
                            row--;

                            var xlAddress = new ExcelAddress(2, 2, row, 2);
                            var conditional = ws.ConditionalFormatting.AddTwoColorScale(xlAddress);

                            conditional.LowValue.Type = eExcelConditionalFormattingValueObjectType.Num;
                            conditional.LowValue.Value = days.Min(x => x.AllMachines);
                            conditional.LowValue.Color = Color.White;

                            conditional.HighValue.Type = eExcelConditionalFormattingValueObjectType.Num;
                            conditional.HighValue.Value = days.Max(x => x.AllMachines);
                            conditional.HighValue.Color = Color.FromArgb(255, 154, 154);
                            conditional.Style.Font.Bold = true;

                            var cellsTableStyle = ws.Cells[1, 1, row, 2].Style;
                            cellsTableStyle.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            cellsTableStyle.Border.Top.Style = ExcelBorderStyle.Thin;
                            cellsTableStyle.Border.Left.Style = ExcelBorderStyle.Thin;
                            cellsTableStyle.Border.Right.Style = ExcelBorderStyle.Thin;
                            cellsTableStyle.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            ws.Row(1).Height = 30;
                            ws.Column(1).AutoFit(10);
                            ws.Column(2).Width = 19;

                            await package.SaveAsAsync(file);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"Ошибка при экспорте:\n\n{ex.Message}", Properties.Resources.Header, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        finally
                        {
                            file?.Close();
                            file?.Dispose();
                            ws.Dispose();
                            package.Dispose();
                        }
                        
                    });

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            
        }

    }
}
