﻿// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections;
using System.Drawing;
using System.Text;

namespace PlanLoad
{
    public static class StringColor
    {
        private static Options _options = new Options();

        public static Color BuildToRgb(string value)
        {
            return ColorToRgb.ToRgb(BuildToHsl(value));
        }

        public static Hsl BuildToHsl(string value)
        {
            double h, s, l;

            var hashGenerator = new BKDRHash();

            var hash = hashGenerator.Generate(value);

            if (_options.HueRanges.Count > 0)
            {
                var rangeIndex = hash % Convert.ToUInt64(_options.HueRanges.Count);

                var hueValue = (Hue)_options.HueRanges[(int)rangeIndex];

                var hueResolution = Convert.ToUInt64(727);
                h = ((hash / Convert.ToUInt64(_options.HueRanges.Count)) % hueResolution)
                    * (Convert.ToUInt64(hueValue.Max) - Convert.ToUInt64(hueValue.Min)) / hueResolution + Convert.ToUInt64(hueValue.Min);

            }
            else
            {
                h = hash % 359;
            }

            hash = (hash / 360);
            var sIndex = (hash % (ulong)_options.S.Count);
            s = (double)_options.S[(int)sIndex];

            hash = (hash / (ulong)_options.S.Count);
            var lIndex = (hash % (ulong)_options.L.Count);
            l = (double)_options.L[(int)lIndex];

            var hslResult = new Hsl(h, s, l);

            return hslResult;
        }

    }
    public class Hsl
    {
        public Hsl()
        {
        }
        public Hsl(double h, double s, double l)
        {
            H = h;
            S = s;
            L = l;
        }
        public double H { get; set; }
        public double S { get; set; }
        public double L { get; set; }
    }
    public class BKDRHash
    {
        private const char PADDING_CHAR = 'x';

        /// <summary>
        /// The Number.MAX_SAFE_INTEGER constant represents the maximum safe integer in JavaScript  
        /// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Number/MAX_SAFE_INTEGER
        /// </summary>
        private const ulong JAVASCRIPT_MAX_SAFE_INTEGER = 9007199254740991;

        private const ulong SEED = 131;

        private const ulong SEED2 = 137;


        public ulong Generate(string value)
        {
            return GenerateVersion3(value);
        }

        public ulong GenerateVersion3(string value)
        {
            ulong hash = 0;

            // Make hash more sensitive for short string like 'a', 'b', 'c'
            var valueWithPadding = $"{value}{PADDING_CHAR}";

            var valueUtf8 = ToUTF8(valueWithPadding);

            var max = (JAVASCRIPT_MAX_SAFE_INTEGER / SEED);

            for (int i = 0; i < valueUtf8.Length; i++)
            {
                if (hash > max)
                {
                    hash = (hash / SEED2);

                }

                var bytes = ToUTF8Bytes(valueUtf8[i].ToString());

                hash = (hash * SEED) + bytes[0];

            }

            return hash;
        }

        private string ToUTF8(string value)
        {
            var bytes = Encoding.Default.GetBytes(value);
            return Encoding.UTF8.GetString(bytes);
        }

        private byte[] ToUTF8Bytes(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            return bytes;
        }
    }
    public class Options
    {
        public ArrayList HueRanges { get; set; }

        /// <summary>
        /// Saturation
        /// </summary>
        public ArrayList S { get; set; }

        /// <summary>
        /// Lightness
        /// </summary>
        public ArrayList L { get; set; }

        public Options()
        {
            S = GetLS(new ArrayList() { 0.35, 0.5, 0.65 });
            L = GetLS(new ArrayList() { 0.35, 0.5, 0.65 });

            HueRanges = new ArrayList();
        }


        /// <summary>
        /// Get lightness or saturation
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        internal protected ArrayList GetLS(double param)
        {
            return new ArrayList() { param };
        }

        /// <summary>
        ///  Get lightness or saturation
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        internal protected ArrayList GetLS(ArrayList param)
        {
            if (param == null)
            {
                // note that 3 is a prime
                param = new ArrayList() { 0.35, 0.5, 0.65 };
            }

            return param;
        }

        internal protected ArrayList GetLS()
        {
            return GetLS(null);
        }


    }
    public class Hue
    {
        public Hue()
        {
            Min = 0;
            Max = 360;
        }
        public int Min { get; set; }
        public int Max { get; set; }
    }
    public static class ColorToRgb
    {
        public static Color ToRgb(Hsl hsl)
        {
            return ToRgb(hsl.H, hsl.S, hsl.L);
        }


        public static Color ToRgb(double hue, double saturation, double lightness)
        {
            return ToRgb2(hue, saturation, lightness);
        }
        public static Color ToRgb1(double hue, double saturation, double lightness)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            lightness = lightness * 255;
            int v = Convert.ToInt32(lightness);
            int p = Convert.ToInt32(lightness * (1 - saturation));
            int q = Convert.ToInt32(lightness * (1 - f * saturation));
            int t = Convert.ToInt32(lightness * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(255, t, p, v);
            else
                return Color.FromArgb(255, v, p, q);
        }

        public static Color ToRgb2(double hue, double saturation, double lightness)
        {
            var h = hue / 360;

            var q = lightness < 0.5
                    ? lightness * (1 + saturation)
                    : lightness + saturation - (lightness * saturation);


            var p = 2.0 * lightness - q;

            var r = GetColor(h + 1 / 3.0, q, p);
            var g = GetColor(h, q, p);
            var b = GetColor(h - 1 / 3.0, q, p);

            return Color.FromArgb(alpha: 255, red: r, green: g, blue: b);


        }

        private static int GetColor(double color, double q, double p)
        {
            if (color < 0)
            {
                color++;
            }

            if (color > 1)
            {
                color--;
            }

            if (color < (1.0 / 6.0))
            {
                color = p + (q - p) * 6.0 * color;
            }
            else if (color < 0.5)
            {
                color = q;
            }
            else if (color < (2.0 / 3.0))
            {
                color = p + (q - p) * 6.0 * ((2.0 / 3.0) - color);
            }
            else
            {
                color = p;
            }

            return (int)Math.Round(color * 255);


        }
    }
}
